# models.py

from sentiment_data import *
import collections
import numpy as np
import math

# Feature extraction base type. Takes an example and returns an indexed list of features.
class FeatureExtractor(object):
    def get_indexer(self):
        raise Exception("Don't call me, call my subclasses")

    # Extract features. Includes a flag add_to_indexer to control whether the indexer should be expanded.
    # At test time, any unseen features should be discarded, but at train time, we probably want to keep growing it.
    def extract_features(self, ex: SentimentExample, add_to_indexer: bool=False):
        raise Exception("Don't call me, call my subclasses")


# Extracts unigram bag-of-words features from a sentence. It's up to you to decide how you want to handle counts
class UnigramFeatureExtractor(FeatureExtractor):
    def __init__(self,name, indexer: Indexer, feats, negfeat, posfeat, numNeg, numPos): #,collections.count()):
        self.name = name
        self.indexer = indexer
        self.negfeat = negfeat
        self.posfeat = posfeat
        self.feats = feats
        self.numNeg = numNeg
        self.numPos = numPos
        
    def extract_features(self, ex: SentimentExample, add_to_indexer: bool):
        self.name = "UNI"
        if add_to_indexer:
            if ex.label == 0:
                self.numNeg += 1
            else: 
                self.numPos += 1
            for word in ex.words:

                #print (type(word))
                self.indexer.add_and_get_index(word)
                if ex.label == 0:
                    maybe_add_feature(self.negfeat, self.indexer, add_to_indexer, word)
                else:
                    maybe_add_feature(self.posfeat, self.indexer, add_to_indexer, word)
        else:
            for word in ex.words:
                maybe_add_feature(self.feats, self.indexer, add_to_indexer, word)
        
# Bigram feature extractor analogous to the unigram one.
class BigramFeatureExtractor(FeatureExtractor):
    def __init__(self, name, indexer: Indexer, feats, negfeat, posfeat, numNeg, numPos): #,collections.count()):
        self.name = name
        self.indexer = indexer
        self.negfeat = negfeat
        self.posfeat = posfeat
        self.feats = feats
        self.numNeg = numNeg
        self.numPos = numPos
    
    def extract_features(self, ex: SentimentExample, add_to_indexer: bool):
        self.name = "BI"
        num = len(ex.words)
        if add_to_indexer:
            for i in range(num):
                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1]
                #print (type(word))
                self.indexer.add_and_get_index(word)
                if ex.label == 0:
                    self.numNeg += 1
                    maybe_add_feature(self.negfeat, self.indexer, add_to_indexer, word)
                else:
                    self.numPos += 1
                    maybe_add_feature(self.posfeat, self.indexer, add_to_indexer, word)
        else:
            for i in range(num):
                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1]
                maybe_add_feature(self.feats, self.indexer, add_to_indexer, word)

# Better feature extractor...try whatever techniques you can think of!
class BetterFeatureExtractor(FeatureExtractor):
    def __init__(self, name, indexer: Indexer, feats, negfeat, posfeat, numNeg, numPos): #,collections.count()):
        self.name = name
        self.indexer = indexer
        self.negfeat = negfeat
        self.posfeat = posfeat
        self.feats = feats
        self.numNeg = numNeg
        self.numPos = numPos
        
    def extract_features(self, ex: SentimentExample, add_to_indexer: bool):
        self.name = "BETTER"
        num = len(ex.words)
        if add_to_indexer:
            for i in range(num):
                word = ex.words[i]
                self.indexer.add_and_get_index(word)
                if ex.label == 0:
                    self.numNeg += 1
                    maybe_add_feature(self.negfeat, self.indexer, add_to_indexer, word)
                else:
                    self.numPos += 1
                    maybe_add_feature(self.posfeat, self.indexer, add_to_indexer, word)

                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1]

                self.indexer.add_and_get_index(word)
                if ex.label == 0:
                    #self.numNeg += 1
                    maybe_add_feature(self.negfeat, self.indexer, add_to_indexer, word)
                else:
                    #self.numPos += 1
                    maybe_add_feature(self.posfeat, self.indexer, add_to_indexer, word)
        else:
            for i in range(num):
                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1]
                maybe_add_feature(self.feats, self.indexer, add_to_indexer, word)

# Sentiment classifier base type
class SentimentClassifier(object):
    # Makes a prediction for the given
    def predict(self, ex: SentimentExample):
        raise Exception("Don't call me, call my subclasses")


# Always predicts the positive class
class TrivialSentimentClassifier(SentimentClassifier):
    def predict(self, ex: SentimentExample):
        return 1


# Implement this class -- you should at least have init() and implement the predict method from the SentimentClassifier
# superclass
class NaiveBayesClassifier(SentimentClassifier):
    def __init__(self, negCont, posCont, numNeg, numPos,  starNegCnt, starPosCnt, indexer , name):
        self.negCont = negCont
        self.posCont = posCont
        self.numNeg = numNeg
        self.numPos = numPos
        self.starNegCnt = starNegCnt
        self.starPosCnt = starPosCnt
        self.indexer = indexer
        self.name = name
    def predict(self, ex: SentimentExample):
        
        proNeg = self.numNeg/(self.numNeg + self.numPos)
        proPos = self.numPos/(self.numNeg + self.numPos)
        
        #p(xi|y) I need total count for every word in the neg/pos (starCnt) 
        #I need the count for that word in the neg/pos (this) (thisCnt)
        predNeg = 1 
        predPos = 1
        
        num = len(ex.words)
        itidfNeg = np.zeros(len(self.negCont)) #all zero vector for all feature for negative class
        itidfPos = np.zeros(len(self.posCont)) #all zero vector for all feature for positive class
        for i in range(num):
            if self.name == "BETTER":
                word = ex.words[i]
                indx = self.indexer.index_of(word)  
                if (indx != -1):
                    thisNegCnt = self.negCont [indx]
                    thisPosCnt = self.posCont [indx]
                    predNeg *= (thisNegCnt + .6) / (self.starNegCnt + .6 *len(self.negCont)) 
                    predPos *= (thisPosCnt + .6) / (self.starPosCnt + .6 *len(self.posCont))
                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1]
            
            if self.name == "UNI":
                word = ex.words[i]
            if self.name == "BI":
                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1] 
            indx = self.indexer.index_of(word)  
            if (indx != -1):
                thisNegCnt = self.negCont [indx]
                thisPosCnt = self.posCont [indx]
                predNeg *= (thisNegCnt +1 ) / (self.starNegCnt+ 1 *len(self.negCont)) 
                predPos *= (thisPosCnt +1) / (self.starPosCnt +1 *len(self.posCont))
        
        predNeg *= proNeg
        predPos *= proPos
        #print ("proneg, proPo ", predNeg, predPos)
        #normalization
        if self.name == "BETTER":
            if predPos > predNeg:
                return 1
            else:
                return 0
        else:
            if predPos/(predPos + predNeg) > predNeg/ (predPos + predNeg):
                return 1
            else:
                return 0
                            


# Implement this class -- you should at least have init() and implement the predict method from the SentimentClassifier
# superclass
class PerceptronClassifier(SentimentClassifier):
    def __init__(self, w, x, indexer, name):
        self.w = w
        self.indexer = indexer
        self.x = x
        self.name = name
    def predict (self, ex: SentimentExample):
        #x is the feature array for the testing sentence
        x = np.zeros(len(self.w))
        
        if self.name == "UNI":
            for word in ex.words:
                x[self.indexer.index_of(word)] =1
            #retrieve the weight and update 
        if self.name == "BI":
            num = len(ex.words)
            for i in range(num):
                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1] 
                x[self.indexer.index_of(word)] =1

        if self.name == "BETTER":
            num = len(ex.words)
            for word in ex.words:
                x[self.indexer.index_of(word)] =1
            for i in range(num):
                if i == 0:
                    word = '<s> ' + ex.words[i]
                elif i == (num -1):
                    word = ex.words[i] + ' </s>'
                else:
                    word = ex.words[i] +' ' + ex.words[i+1] 
                x[self.indexer.index_of(word)] =1

        ypred = np.dot(self.w, x)
        if ypred >= 0:
            return 1 
        else:
            return 0



# Train a Naive Bayes model on the given training examples using the given FeatureExtractor
def train_nb(train_exs: List[SentimentExample], feat_extractor: FeatureExtractor) -> NaiveBayesClassifier:
    name = feat_extractor.name
    for ex in train_exs:
        feat_extractor.extract_features(ex,True)
    numNeg = feat_extractor.numNeg  # num of documents that are negative
    numPos = feat_extractor.numPos  # num of documents that are positive
    negCont = []
    posCont = []
    negCont = collections.Counter(feat_extractor.negfeat)
    posCont = collections.Counter(feat_extractor.posfeat)
    starNegCnt = len(feat_extractor.negfeat) #all words counts for all negative labels
    starPosCnt = len(feat_extractor.posfeat) #all words counts for all positive labels

    return NaiveBayesClassifier(negCont, posCont, numNeg, numPos,starNegCnt, starPosCnt, feat_extractor.indexer, name)
    
# Train a Perceptron model on the given training examples using the given FeatureExtractor
def train_perceptron(train_exs: List[SentimentExample], feat_extractor: FeatureExtractor) -> PerceptronClassifier:
    name = feat_extractor.name
    for ex in train_exs:
        feat_extractor.extract_features(ex,True)
    numNeg = feat_extractor.numNeg
    numPos = feat_extractor.numPos
    featCont = []
    featCont = collections.Counter(feat_extractor.negfeat)
    featCont.update(feat_extractor.posfeat)
    w = np.zeros(len(featCont)+1) #initial weights of all 0
    
    if name == "BI" :
        for i in range(4):
            for ex in train_exs:
                #xi is the count of word for each trainning sentence
                xi = np.zeros(len(featCont)+1)
                num = len(ex.words)
                for j in range(num):
                    if j == 0:
                        word = '<s> ' + ex.words[j]
                    elif j == (num -1):
                        word = ex.words[j] + ' </s>'
                    else:
                        word = ex.words[j] +' ' + ex.words[j+1] 
                    if feat_extractor.indexer.index_of(word)!= -1:
                        xi [feat_extractor.indexer.index_of(word)] += 1
                xi[:1] = 1
                ypred = np.sign(np.dot(w,xi))
                if ypred >= 0 :
                    if ex.label == 0:
                        w = w - 1/(i+5) * xi
                else:
                    if ex.label == 1:
                        w = w + 1/(i+5) * xi
                
    if name == "UNI":
        for i in range(4):
            for ex in train_exs:
                #xi is the count of word for each trainning sentence
                xi = np.zeros(len(featCont)+1)
                #print ("start:" , xi)
                
                for word in ex.words:
                    if feat_extractor.indexer.index_of(word)!= -1:
                        xi [feat_extractor.indexer.index_of(word)] += 1
                xi[:1] = 1
                ypred = np.sign(np.dot(w,xi))
                if ypred >= 0 :
                    if ex.label == 0:
                        w = w - 1/(i+5) * xi
                else:
                    if ex.label == 1:
                        w = w + 1/(i+5) * xi
    if name == "BETTER" :
        for i in range(4):
            for ex in train_exs:
                #xi is the count of word for each trainning sentence
                xi = np.zeros(len(featCont)+1)
                for word in ex.words:
                    if feat_extractor.indexer.index_of(word)!= -1:
                        xi [feat_extractor.indexer.index_of(word)] += 1
                num = len(ex.words)
                for j in range(num):
                    if j == 0:
                        word = '<s> ' + ex.words[j]
                    elif j == (num -1):
                        word = ex.words[j] + ' </s>'
                    else:
                        word = ex.words[j] +' ' + ex.words[j+1] 
                    if feat_extractor.indexer.index_of(word)!= -1:
                        xi [feat_extractor.indexer.index_of(word)] += 1
                xi[:1] = 1
                ypred = np.sign(np.dot(w,xi))
                if ypred >= 0 :
                    if ex.label == 0:
                        w = w - 1/(i+5) * xi
                else:
                    if ex.label == 1:
                        w = w + 1/(i+5) * xi

    x = np.zeros(w.size)
    #b = w
    #ind = np.argpartition(b, 10)[:10]
    #print (ind)
    #b= [ 797,  321, 4570,  620, 3060,  205,  408, 3208,  332,  278]
    #for i in b:
    #    print (feat_extractor.indexer.get_object(i))
    return PerceptronClassifier(w, x, feat_extractor.indexer, name)



# Main entry point for your modifications. Trains and returns one of several models depending on the args
# passed in from the main method.
def train_model(args, train_exs):
    # Initialize feature extractor
    if args.feats == "UNIGRAM":
        # Add additional preprocessing code here
        negfeat = []
        posfeat = []
        feats = []
        numNeg = 0 
        numPos = 0
        name = "UNI"
        feat_extractor = UnigramFeatureExtractor(name, Indexer(),  feats, negfeat, posfeat, numNeg, numPos)
    elif args.feats == "BIGRAM":
        # Add additional preprocessing code here
        negfeat = []
        posfeat = []
        feats = []
        numNeg = 0 
        numPos = 0
        name = "BI"
        feat_extractor = BigramFeatureExtractor(name, Indexer(), feats, negfeat, posfeat, numNeg, numPos)
    elif args.feats == "BETTER":
        # Add additional preprocessing code here
        negfeat = []
        posfeat = []
        feats = []
        numNeg = 0 
        numPos = 0
        name = "BETTER"
        feat_extractor = BetterFeatureExtractor(name, Indexer(), feats, negfeat, posfeat, numNeg, numPos)
    else:
        raise Exception("Pass in UNIGRAM, BIGRAM, or BETTER to run the appropriate system")

    # Train the model
    if args.model == "TRIVIAL":
        model = TrivialSentimentClassifier()
    elif args.model == "NB":
        model = train_nb(train_exs, feat_extractor)
    elif args.model == "PERCEPTRON":
        model = train_perceptron(train_exs, feat_extractor)
    else:
        raise Exception("Pass in TRIVIAL, NB, or PERCEPTRON to run the appropriate system")
    return model